function server_manager.warn_player(name, msg)
    minetest.chat_send_player(name, minetest.colorize("#e6482e", "[!] " .. msg))
end



function server_manager.warn_server(msg)
    minetest.chat_send_all("\n[!!] " .. minetest.colorize("#eea160", msg .. "\n"))
end



function server_manager.get_seconds_time()
    return minetest.get_us_time() / 1000000
end