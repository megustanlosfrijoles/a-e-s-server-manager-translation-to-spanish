local S = minetest.get_translator("server_manager")

local DELAY = 600
local broadcast_index = 1
local messages = {
  "\n[Server] " .. minetest.colorize("#eea160", S("Got some ideas about how to improve the server? Go to the main square, write us a book and put it into the suggestion box!") .. "\n"),
  "\n[Server] " .. minetest.colorize("#eea160", S("Why did we choose Minetest instead of Minecraft? Because it's free software!").. "\n"),
  "\n[Server] " .. minetest.colorize("#eea160", S("Do you want to develop your own minigame? We made a library to help you out: search for 'arena_lib' on GitLab and... maybe one day we'll also feature it here!").. "\n"),
  "\n[Server] " .. minetest.colorize("#eea160", S("Do you want to support what we do (server and mods)? Offer us a coffee on liberapay.com/aes_minetest_server").. "\n"),
  "\n[Server] " .. minetest.colorize("#eea160", S("Playing while in a call is way more fun than alone; and we have a Mumble server for that! Join us at mumble.syntheticstars.org").. "\n"),
  "\n[Server] " .. minetest.colorize("#eea160", S("Looking for people to play with or you want to follow the evolution of the server? Take part in our Matrix community: #arcadeemulationserver:matrix.org (you need a client like Element)").. "\n"),
  "\n[Server] " .. minetest.colorize("#eea160", S("Do you like our minigames? Leave a review on content.minetest.net!").. "\n")
}


local function main()

    broadcast_index = broadcast_index +1

    if broadcast_index > #messages then
      broadcast_index = 1
    end

    minetest.chat_send_all(messages[broadcast_index])
    minetest.after(DELAY, main)

end

minetest.after(DELAY, main)
