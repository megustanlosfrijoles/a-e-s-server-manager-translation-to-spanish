local function teleport_to_player(name, target_name) end
local S = minetest.get_translator("server_manager")



ChatCmdBuilder.new("tp", function(cmd)
    cmd:sub(":target", function(name, target_name)
        minetest.chat_send_player(name, teleport_to_player(name, target_name))
    end)
    cmd:sub(":playername :target", function(name, pl_name, target_name)
        minetest.chat_send_player(name, teleport_to_player(pl_name, target_name))
    end)
end, {
  description = "tp <player> [to_player]",
  privs = { teleport = true  }
})



function teleport_to_player(name, target_name)
	if name == target_name then
		return "One does not teleport to oneself."
	end
	local teleportee = minetest.get_player_by_name(name)
	if not teleportee then
		return S("Cannot get teleportee with name @1.", name)
	end
	if teleportee:get_attach() then
		return S("Cannot teleport, @1 is attached to an object!", name)
	end
	local target = minetest.get_player_by_name(target_name)
	if not target then
		return S("Cannot get target player with name @1.", target_name)
	end

	local p = target:get_pos()
	teleportee:set_pos(p)
	return S("Teleporting @1 to @2 at @3.", name, target_name,
    minetest.pos_to_string(p, 1))
end
