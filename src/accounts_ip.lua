ChatCmdBuilder.new("accounts", function(cmd)
	cmd:sub(":target", function(name, target_name)
		local accounts = {}
		local original_ip = minetest.get_player_information(target_name).address

		for _, obj in ipairs(minetest.get_connected_players()) do
			local name = obj:get_player_name()
			local ip = minetest.get_player_information(name).address

			if original_ip == ip then
				table.insert(accounts, name)
			end
		end

		minetest.chat_send_player(name, target_name.."'s accounts: "..table.concat(accounts, ", "))
	end)
end, {
 description = "accounts <player>: it prints out all the connected players with the same ip",
 privs = { server = true  }
})