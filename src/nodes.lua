local S = minetest.get_translator("server_manager")

minetest.register_node("server_manager:barrier", {
    description = S("Barrier"),
    drawtype = "airlike",
    paramtype = "light",
    sunlight_propagates = true,
    air_equivalent = true,
    drop = "",
    inventory_image = "servermanager_barrier.png",
    wield_image = "servermanager_barrier.png"
})

minetest.register_alias("barrier", "server_manager:barrier")
