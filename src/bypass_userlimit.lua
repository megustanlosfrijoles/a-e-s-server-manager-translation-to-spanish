local exceptions = {
  "_Zaizen_",
  "Zughy",
  "Giov4",
  "XxCrazyMinerxX",
  "SonoMichele"
}

minetest.register_can_bypass_userlimit(function(name, ip)
  return table.indexof(exceptions, name) > 0
end)
