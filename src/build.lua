function minetest.is_protected(pos, name)
  if minetest.check_player_privs(name, {build = true}) then
    return false
  else
    return true
  end
end

minetest.register_privilege("build", {
    description = "Can build",
    give_to_singleplayer = false,
    give_to_admin = true
})
