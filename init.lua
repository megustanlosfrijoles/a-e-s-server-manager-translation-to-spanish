server_manager = {}

local srcpath = minetest.get_modpath("server_manager") .. "/src"


dofile(srcpath .. "/libs/chatcmdbuilder.lua")

dofile(srcpath .. "/broadcast.lua")
dofile(srcpath .. "/build.lua")
dofile(srcpath .. "/bypass_userlimit.lua")
dofile(srcpath .. "/cmd_aliases.lua")
dofile(srcpath .. "/nodes.lua")
dofile(srcpath .. "/player_manager.lua")
dofile(srcpath .. "/privs.lua")
dofile(srcpath .. "/utils.lua")
dofile(srcpath .. "/chat_moderation/caps_prevention.lua")
dofile(srcpath .. "/chat_moderation/chat_log.lua")
dofile(srcpath .. "/chat_moderation/flood_prevention.lua")
dofile(srcpath .. "/chat_moderation/mute.lua")
dofile(srcpath .. "/accounts_ip.lua")
